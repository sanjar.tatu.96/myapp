package uz.pdp.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import uz.pdp.myapplication.location.GetPathFromLocation;
import uz.pdp.myapplication.models.LocationData;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {
    private RecyclerView commentList;
    private ArrayList<String> comments;
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation = null;
    Marker mCurrLocationMarker;
    View bottomLayout;
    ArrayList<LocationData> locationData;
    Map<String, String> markerMap = new HashMap<>();

    View call_btn;
    private Marker lastMarker = null;
    private GoogleMap mMap;
    private LinearLayout showComment;
    private ConstraintLayout show_l;
    private boolean show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_maps);
        bottomLayout = findViewById(R.id.bottom_layout);
        call_btn = findViewById(R.id.call_btn);
        loadLocationData();

        loadComment();
        show_l = findViewById(R.id.show_comment);
        showComment = findViewById(R.id.comments_btn);
        showComment.setOnClickListener(v -> {
            if (!show) {
                show_l.setVisibility(View.VISIBLE);
                show = true;
            } else {
                show = false;
                show_l.setVisibility(View.GONE);
            }
        });

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        call_btn.setOnClickListener(v -> {
            mGoogleMap.addPolyline(new PolylineOptions());
            if (lastMarker.getTitle().equals("AVTOOMAD MChJ")) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                new GetPathFromLocation(latLng, lastMarker.getPosition(), polyLine -> mGoogleMap.addPolyline(polyLine), loadJSONFromAsset("json1.json")).execute();
            } else if (lastMarker.getTitle().equals("SARHUN-SERVIS MChJ")) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                new GetPathFromLocation(latLng, lastMarker.getPosition(), polyLine -> mGoogleMap.addPolyline(polyLine), loadJSONFromAsset("json2.json")).execute();
            } else if (lastMarker.getTitle().equals("O'ZAVTOSANOATTRANS MChJ")) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                new GetPathFromLocation(latLng, lastMarker.getPosition(), polyLine -> mGoogleMap.addPolyline(polyLine), loadJSONFromAsset("json3.json")).execute();
            } else if (lastMarker.getTitle().equals("BADIS-TEXNO-SERVIS M.Ch.J.")) {
                LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                new GetPathFromLocation(latLng, lastMarker.getPosition(), polyLine -> mGoogleMap.addPolyline(polyLine), loadJSONFromAsset("json4.json")).execute();
            }
            bottomLayout.setTranslationY(437);
        });
    }

    private void loadComment() {

    }


    private void loadLocationData() {
        locationData.add(new LocationData(new LatLng(41.337869, 69.340654), "AVTOOMAD MChJ", ""));
        locationData.add(new LocationData(new LatLng(41.3432867, 69.331581), "SARHUN-SERVIS MChJ", ""));
        locationData.add(new LocationData(new LatLng(41.3312299, 69.3283188), "O'ZAVTOSANOATTRANS MChJ", ""));
        locationData.add(new LocationData(new LatLng(41.3273828, 69.3314409), "BADIS-TEXNO-SERVIS M.Ch.J.", ""));

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        bottomLayout.setVisibility(View.GONE);
        locationsLoaded(locationData);
        mGoogleMap.setOnInfoWindowClickListener(this::onInfoWindowClick);
        mGoogleMap.setOnMarkerClickListener(this::onMarkerClick);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        bottomLayout.setTranslationY(0);
        String markerId = marker.getTitle();
        if (lastMarker == null) {
            if (markerId.equals("AVTOOMAD MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("SARHUN-SERVIS MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("O'ZAVTOSANOATTRANS MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("BADIS-TEXNO-SERVIS M.Ch.J.")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            }
        } else {
            lastMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.service_location));
            if (markerId.equals("AVTOOMAD MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("SARHUN-SERVIS MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("O'ZAVTOSANOATTRANS MChJ")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            } else if (markerId.equals("BADIS-TEXNO-SERVIS M.Ch.J.")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_service_location));
                lastMarker = marker;
            }
        }
        Animation animation = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
        animation.setFillAfter(true);
        animation.setDuration(1000);
        bottomLayout.setAnimation(animation);
        bottomLayout.setVisibility(View.VISIBLE);


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        bottomLayout.setVisibility(View.GONE);
        bottomLayout.setAnimation(null);
        return false;
    }

    private void locationsLoaded(List<LocationData> locations) {
        for (LocationData myLoc : locations) {
            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(myLoc.getLatLng())
                    .title(myLoc.getTitle())
                    .snippet(myLoc.getSnippet())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.service_location)));
            markerMap.put(marker.getId(), myLoc.getTitle());
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", (dialogInterface, i) ->
                                ActivityCompat.requestPermissions(ServiceMapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION))
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}

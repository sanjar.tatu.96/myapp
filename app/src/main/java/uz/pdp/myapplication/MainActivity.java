package uz.pdp.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import uz.pdp.myapplication.fragments.SingInFragment;
import uz.pdp.myapplication.fragments.SingUpFragment;

public class MainActivity extends AppCompatActivity {


    Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        loadFragment();

    }

    private void loadFragment() {
        SingUpFragment singUpFragment = new SingUpFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, singUpFragment);
        transaction.commit();

    }
}

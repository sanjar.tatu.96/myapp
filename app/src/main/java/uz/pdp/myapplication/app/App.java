package uz.pdp.myapplication.app;

import android.app.Application;
import uz.pdp.myapplication.db.database.AppDataBase;
import uz.pdp.myapplication.db.entity.User;


import java.util.ArrayList;

public class App extends Application {
    private AppDataBase dataBase;
    public static User user;

    public static App app;
    public int a;

    public static App getApp() {
        if (app == null) {
            app = new App();
            app.onCreate();
        }
        return app;
    }

    public AppDataBase getDataBase() {
        return dataBase;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();

    }

    private void init() {

        dataBase = AppDataBase.getAppDatabase(this);


    }


    public boolean isUser(String carNUmber, String password) {
        dataBase = AppDataBase.getAppDatabase(this);
        User[] users = dataBase.userDao().loadAll();
        if (users != null)
            for (int i = 0; i < users.length; i++) {
                if (users[i].carNumber.equals(carNUmber) && users[i].password.equals(password)) {
                    user = users[i];
                    return true;
                }
            }
        return false;
    }


}

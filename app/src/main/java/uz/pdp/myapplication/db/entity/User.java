package uz.pdp.myapplication.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User {
    public User(String fullName, String carType, String carNumber, String carOil, String phoneNumber, String password) {
        this.fullName = fullName;
        this.carType = carType;
        this.carNumber = carNumber;
        this.carOil = carOil;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    @PrimaryKey(autoGenerate = true)
    public int userId;
    public String fullName;
    public String carType;
    public String carNumber;
    public String carOil;
    public String phoneNumber;
    public String password;

}

package uz.pdp.myapplication.db.util;


import uz.pdp.myapplication.db.database.AppDataBase;
import uz.pdp.myapplication.db.entity.User;

public class AddDataBase {
    private static AddDataBase addDataBase;
    private static AppDataBase dataBase;

    public static AddDataBase with(AppDataBase appDataBase) {
        if (dataBase == null) {
            dataBase = appDataBase;
        }
        if (addDataBase == null)
            addDataBase = new AddDataBase();
        return addDataBase;
    }

    public void addHall(String fullName, String carType, String carNumber, String carOil, String phoneNumber, String password) {
        User hall = new User(fullName, carType, carNumber, carOil, phoneNumber, password);

        dataBase.userDao().insert(hall);
    }

}

package uz.pdp.myapplication.db.dao;

import android.arch.persistence.room.*;
import uz.pdp.myapplication.db.entity.User;


@Dao
public interface UserDao {

    @Insert
    void insert(User hall);

    @Delete
    void delete(User hall);

    @Update
    void update(User hall);

    @Query("Select * FROM User")
    User[] loadAll();
}

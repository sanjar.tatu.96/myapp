package uz.pdp.myapplication.db.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import uz.pdp.myapplication.db.dao.UserDao;
import uz.pdp.myapplication.db.entity.User;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {
    private static AppDataBase insert;


    public abstract UserDao userDao();

    public static AppDataBase getAppDatabase(Context context) {
        if (insert == null) {
            insert = Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, "kassa-db")
                    .allowMainThreadQueries()
                    .build();

        }
        return insert;

    }

}

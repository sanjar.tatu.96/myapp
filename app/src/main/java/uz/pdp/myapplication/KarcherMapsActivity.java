package uz.pdp.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import uz.pdp.myapplication.models.LocationData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KarcherMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation = null;
    Marker mCurrLocationMarker;
    View bottomLayout;
    ArrayList<LocationData> locationData;
    Map<String, String> markerMap = new HashMap<>();

    View call_btn;
    private Marker lastMarker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karcher_maps);
        bottomLayout = findViewById(R.id.bottom_layout);
        call_btn = findViewById(R.id.call_btn);
        loadLocationData();

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        call_btn.setOnClickListener(v -> {
            Log.d(mLastLocation.getLatitude() + "", "------" + mLastLocation.getLongitude());
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            //new GetPathFromLocation(latLng, lastMarker.getPosition(), polyLine -> mGoogleMap.addPolyline(polyLine)).execute();
            bottomLayout.setTranslationY(437);
        });
    }

    private void loadLocationData() {
        locationData = new ArrayList<>();
        locationData.add(new LocationData(new LatLng(41.3234147, 69.3266451), "Avtomoyka", ""));
        locationData.add(new LocationData(new LatLng(41.3242326, 69.3221604), "Avtomoyka", ""));
        locationData.add(new LocationData(new LatLng(41.3242326, 69.3221604), "Avtomoyka", ""));
        locationData.add(new LocationData(new LatLng(41.3312299, 69.3283188), "Avtomoyka", ""));
        locationData.add(new LocationData(new LatLng(41.3273828, 69.3314409), "Avtomoyka", ""));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        bottomLayout.setVisibility(View.GONE);
        locationsLoaded(locationData);
        mGoogleMap.setOnInfoWindowClickListener(this::onInfoWindowClick);
        mGoogleMap.setOnMarkerClickListener(this::onMarkerClick);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        bottomLayout.setTranslationY(0);
        String markerId = marker.getTitle();
        if (lastMarker == null) {
            if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            }
        } else {
            lastMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.karcher_location));
            if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            } else if (markerId.equals("Avtomoyka")) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.check_karcher_location));
                lastMarker = marker;
            }
        }
        Animation animation = new TranslateAnimation(Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
        animation.setFillAfter(true);
        animation.setDuration(1000);
        bottomLayout.setAnimation(animation);
        bottomLayout.setVisibility(View.VISIBLE);


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        bottomLayout.setVisibility(View.GONE);
        bottomLayout.setAnimation(null);
        return false;
    }

    private void locationsLoaded(List<LocationData> locations) {
        for (LocationData myLoc : locations) {
            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(myLoc.getLatLng())
                    .title(myLoc.getTitle())
                    .snippet(myLoc.getSnippet())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.karcher_location)));
            markerMap.put(marker.getId(), myLoc.getTitle());
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", (dialogInterface, i) ->
                                ActivityCompat.requestPermissions(KarcherMapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION))
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


}

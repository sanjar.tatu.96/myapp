package uz.pdp.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import uz.pdp.myapplication.*;
import uz.pdp.myapplication.app.App;

public class HomeFragment extends Fragment {
    private ImageView park, service, oil, karcher;
    private TextView carNumber, fullName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_page, container, false);
        fullName = view.findViewById(R.id.fullname);

        carNumber = view.findViewById(R.id.carnumber);
        carNumber.setText(App.user.carNumber);
        fullName.setText(App.user.fullName);
        park = view.findViewById(R.id.park);
        service = view.findViewById(R.id.service);
        oil = view.findViewById(R.id.oil);
        karcher = view.findViewById(R.id.carcher);


        park.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), MapsActivity.class));
        });
        service.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), ServiceMapsActivity.class));
        });
        oil.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), OilMapsActivity.class));
        });
        karcher.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), KarcherMapsActivity.class));
        });

        return view;
    }

}

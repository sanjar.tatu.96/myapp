package uz.pdp.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import uz.pdp.myapplication.R;
import uz.pdp.myapplication.db.database.AppDataBase;
import uz.pdp.myapplication.db.entity.User;

import java.util.ArrayList;

public class SingInFragment extends Fragment {
    //    @BindView(R.id.spinner)
    Spinner spinner;
    TextView singup;
    Button ok;
    EditText fullUsername, modelCar, numberAvto, phone, password1, password2;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(container.getContext()).inflate(R.layout.singin_fragment, container, false);
//        ButterKnife.bind(view);
        ok = view.findViewById(R.id.ok);
        spinner = view.findViewById(R.id.spinner);
        loadSpinner();
        singup = view.findViewById(R.id.singup);

        fullUsername = view.findViewById(R.id.fullname);
        modelCar = view.findViewById(R.id.model);
        numberAvto = view.findViewById(R.id.avtonumber);
        phone = view.findViewById(R.id.phone);
        password1 = view.findViewById(R.id.password1);
        password2 = view.findViewById(R.id.password2);
        singup.setOnClickListener(v -> {
            SingUpFragment singUpFragment = new SingUpFragment();
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container, singUpFragment);
            transaction.commit();

        });
        ok.setOnClickListener(v -> {
            if (password1.getText().toString().equals(password2.getText().toString()) && !password1.getText().toString().equals("")) {
                loadDb();
                SingUpFragment singUpFragment = new SingUpFragment();
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.container, singUpFragment);
                transaction.commit();
            } else {
                Toast.makeText(getContext(), "Password Error !", Toast.LENGTH_SHORT).show();
            }

        });
        return view;
    }

    private void loadDb() {
        String text = spinner.getSelectedItem().toString();
        AppDataBase.getAppDatabase(getContext()).userDao().insert(new User(fullUsername.getText().toString(), modelCar.getText().toString(), numberAvto.getText().toString(), text, phone.getText().toString(), password1.getText().toString()));
    }

    private void loadSpinner() {
        ArrayList<String> item_spinner = new ArrayList<>();
        item_spinner.add("Бензин");
        item_spinner.add("Пропан");
        item_spinner.add("Метан");
        item_spinner.add("Дизель");
        item_spinner.add("Электричество");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.item_spinner, item_spinner);
        spinner.setAdapter(adapter);

    }
}

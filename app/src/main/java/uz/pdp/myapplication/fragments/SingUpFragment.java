package uz.pdp.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import uz.pdp.myapplication.R;
import uz.pdp.myapplication.app.App;

public class SingUpFragment extends Fragment {
    TextView singIn;
    Button button;
    EditText carNumber, password;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.singup_fragment, container, false);
        singIn = view.findViewById(R.id.singin);
        carNumber = view.findViewById(R.id.carsnumber);
        password = view.findViewById(R.id.password);
        singIn.setOnClickListener(v -> {
            SingInFragment singInFragment = new SingInFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.container, singInFragment);
            transaction.commit();
        });
        button = view.findViewById(R.id.open);
        button.setOnClickListener(v -> {

            if (App.getApp().isUser(carNumber.getText().toString().trim(), password.getText().toString().trim())) {
                startActivity(new Intent(getContext(), HomeActivity.class));

            } else Toast.makeText(getContext(), "Error Login or Password !", Toast.LENGTH_SHORT).show();
        });
        return view;
    }
}

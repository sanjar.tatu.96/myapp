package uz.pdp.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import uz.pdp.myapplication.*;
import uz.pdp.myapplication.app.App;

public class HomeActivity extends AppCompatActivity {
    private ImageView park, service, oil, karcher;
    private TextView carNumber, fullName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);
        fullName = findViewById(R.id.fullname);

        carNumber = findViewById(R.id.carnumber);
        carNumber.setText(App.user.carNumber);
        fullName.setText(App.user.fullName);
        park = findViewById(R.id.park);
        service = findViewById(R.id.service);
        oil = findViewById(R.id.oil);
        karcher = findViewById(R.id.carcher);


        park.setOnClickListener(v -> {
            startActivity(new Intent(this, MapsActivity.class));
        });
        service.setOnClickListener(v -> {
            startActivity(new Intent(this, ServiceMapsActivity.class));
        });
        oil.setOnClickListener(v -> {
            startActivity(new Intent(this, OilMapsActivity.class));
        });
        karcher.setOnClickListener(v -> {
            startActivity(new Intent(this, KarcherMapsActivity.class));
        });

    }
}
